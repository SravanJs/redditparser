package com.qburst.redditParse.thread;

import java.net.URL;

import static com.qburst.redditParse.utils.RedditJsonParser.jsonObjectParser;

/**
 * Created by sravan on 12/5/15.
 */
public class DomainThread extends Thread {
    private final URL domainurl;

    public DomainThread(URL domainurl) {
        this.domainurl=domainurl;
    }

    public void run() {
        jsonObjectParser(domainurl, "");
        System.out.println("\nCOMPLETED\n");
    }
}
