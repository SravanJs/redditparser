package com.qburst.redditParse;


import com.qburst.redditParse.resource.RedditResource;
import com.qburst.redditParse.utils.DomainJsonCreator;
import com.qburst.redditParse.utils.InputJsonParser;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

/**
 * Created by sravan on 6/5/15.
 */

public class RedditApplication extends Application<RedditConfiguration> {
    public static void main(String[] args) throws Exception {
        new RedditApplication().run(args);
    }

    @Override
    public void run(RedditConfiguration configuration, Environment environment) throws Exception {
        InputJsonParser.inputParser();
        DomainJsonCreator domainJsonCreator=new DomainJsonCreator();
        domainJsonCreator.jsonObject.put("domain", domainJsonCreator.domainList);
        environment.jersey().register(new RedditResource());
    }
}
