package com.qburst.redditParse.resource;

import com.qburst.redditParse.utils.DomainJsonCreator;
import com.qburst.redditParse.utils.LinkJsonCreator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by sravan on 7/5/15.
 */


@Path("/")
public class RedditResource {
    /*private JSONObject jsonObject;

    public RedditResource(JSONObject jsonObject) {
        this.jsonObject=jsonObject;
    }*/

    @Path("/redditDomains")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public String domains() throws IOException {
        return DomainJsonCreator.jsonObject.toString();
    }

    @Path("/redditLinks")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public String links() throws IOException {
        return LinkJsonCreator.jsonObject.toString();
    }
}
