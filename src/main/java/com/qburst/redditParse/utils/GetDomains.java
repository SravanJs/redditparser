package com.qburst.redditParse.utils;

import com.qburst.redditParse.thread.DomainThread;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

import static com.qburst.redditParse.utils.RedditJsonParser.jsonObjectParser;

/**
 * Created by sravan on 7/5/15.
 */
public class GetDomains {

    static List<String> domainList = new ArrayList<String>();
    static List<String> urlList = new ArrayList<String>();
    static int maxCoreThread=10;
    static int maxPoolSize=20;
    static ThreadPoolExecutor domainExecutor;

    public static void init(){

        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(500);
        domainExecutor = new ThreadPoolExecutor(maxCoreThread, maxPoolSize, 50, TimeUnit.MICROSECONDS, blockingQueue);
        domainExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r,
                                          ThreadPoolExecutor executor) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                executor.execute(r);
            }
        });
    }

    public static void finish(){
        domainExecutor.shutdown();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while(domainExecutor.getActiveCount() > 0);
    }

    public static void getDomainsFromNews(URL urlbase, String afterurl){
        try {

            Date previousDate = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
            URL url = new URL(urlbase + afterurl);
            System.out.println("URL:=" + url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", " ");
            conn.setRequestProperty("User-Agent", "Sravan Js");


            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                JSONObject json = new JSONObject(output);
                JSONObject data = json.getJSONObject("data");
                JSONArray children = data.getJSONArray("children");

                for (int i = 1; i < children.length(); i++) {
                    long redditTime = children.getJSONObject(i).getJSONObject("data").getLong("created");
                    Date createdDate = new Date(redditTime * 1000L);

                    if (createdDate.compareTo(previousDate) < 0) return;
                    String domain = children.getJSONObject(i).getJSONObject("data").getString("domain");
                    String link = children.getJSONObject(i).getJSONObject("data").getString("url");
                    if (domainList.contains(domain) || domain.contains("self") || domain.contains("twitter") || domain.contains("imgur")
                            || domain.contains("youtu") || domain.contains("(pbs.twimg.com)")) continue;

                    domainList.add(domain);
                    urlList.add(link);

                    DomainJsonCreator domainJsonCreator = new DomainJsonCreator();
                    domainJsonCreator.WriteJson(domain, "domainList.json");
                    LinkJsonCreator linkJsonCreator = new LinkJsonCreator();
                    linkJsonCreator.writeJson(link, "linkList.json");

                    URL domainurl = new URL("http://www.reddit.com/domain/"+domain+"/.json?");
                    domainExecutor.execute(new DomainThread(domainurl));
                    System.out.println("\tdomain:\t" + domain + "\tcreated Date\t:" + createdDate);
                }

                Object after = data.get("after");
                if (after.toString() != "null") {
                    String newafterurl = "&after=" + after;
                    getDomainsFromNews(urlbase, newafterurl);
                } else {
                    System.out.println("\nOVER");
                    return;
                }
            }
            conn.disconnect();
        }catch(MalformedURLException e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }catch(RuntimeException e){
            e.printStackTrace();
        }
    }
}
