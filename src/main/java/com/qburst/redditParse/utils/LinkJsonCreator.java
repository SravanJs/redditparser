package com.qburst.redditParse.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by sravan on 12/5/15.
 */
public class LinkJsonCreator {
    public static JSONObject jsonObject = new JSONObject();
    public static JSONArray LinkList = new JSONArray();

    void writeJson(String s, String fName) {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fName, false)));
            LinkList.add(s);
            jsonObject.put("domain", LinkList);
            out.println(jsonObject);
            out.close();
        } catch (java.io.IOException E) {

        }

    }
}
