package com.qburst.redditParse.utils;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import au.com.bytecode.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sravan on 6/5/15.
 */


public class RedditJsonParser {

    static private CSVWriter writer;

    public static void jsonObjectParser(URL urlbase, String afterurl){
        try {
            Date previousDate=new Date(System.currentTimeMillis()-36*60*60*1000);
            URL url=new URL(urlbase+afterurl);
            System.out.println("URL:="+url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", " ");
            conn.setRequestProperty("User-Agent", "Sravan Js");


            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                JSONObject json = new JSONObject(output);
                JSONObject data = json.getJSONObject("data");
                JSONArray children = data.getJSONArray("children");


                writer =  new CSVWriter(new FileWriter("redditData.csv",true), '\t');
                for (int i = 0; i < children.length(); i++) {
                    long redditTime=children.getJSONObject(i).getJSONObject("data").getLong("created");
                    Date createdDate = new Date(redditTime*1000L);
                    if(createdDate.compareTo(previousDate)<0) return;
                    String[] redditData = {children.getJSONObject(i).getJSONObject("data").getString("title"),children.getJSONObject(i).getJSONObject("data").getString("url")};
                    synchronized (writer) {
                        System.out.println("\ndone!"+redditData[0]+redditData[1]);
                        writer.writeNext(redditData);
                    }
                }

                writer.close();

                Object after=data.get("after");
                if (after.toString()!="null") {
                    String newafterurl = "&after=" + after;
                     jsonObjectParser(urlbase, newafterurl);
                }
                else return;
            }
            conn.disconnect();
        }catch(MalformedURLException e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
