package com.qburst.redditParse.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by sravan on 7/5/15.
 */

public class DomainJsonCreator {

    public static JSONObject jsonObject = new JSONObject();
    public static JSONArray domainList = new JSONArray();

    void WriteJson(String s, String fName) {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fName, false)));
            domainList.add(s);
            jsonObject.put("domain", domainList);
            out.println(jsonObject);
            out.close();
        } catch (java.io.IOException E) {

        }

    }
}
