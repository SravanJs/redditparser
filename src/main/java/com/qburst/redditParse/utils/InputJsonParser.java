package com.qburst.redditParse.utils;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

/**
 * Created by sravan on 7/5/15.
 */

public class InputJsonParser {
    public static void inputParser() throws IOException {

        URL url;
        String urlstring;
          BufferedReader br = new BufferedReader(new FileReader("input.json"));
        String sCurrentLine="",full="";
        while ((sCurrentLine = br.readLine()) != null) {
            full=full+sCurrentLine;
        }
        JSONObject json=new JSONObject(full);
        JSONArray urlArray=json.getJSONArray("url");
        GetDomains.init();
        for (int i = 0; i < urlArray.length(); i++) {
            urlstring=urlArray.getString(i);
            url=new URL(urlstring);
            GetDomains.getDomainsFromNews(url, "");
        }
        GetDomains.finish();
    }
}



